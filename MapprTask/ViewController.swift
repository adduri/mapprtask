//
//  ViewController.swift
//  MapprTask
//
//  Created by Nandu Psycho on 10/10/18.
//  Copyright © 2018 Test. All rights reserved.
//

import UIKit

class ViewController: UIViewController,UITableViewDelegate,UITableViewDataSource,UISearchBarDelegate {
    
    var searchResultsArray = [AnyObject]()
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return searchResultsArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:GlobalTableViewCell = (self.searchTableView.dequeueReusableCell(withIdentifier: "search") as! GlobalTableViewCell?)!
        cell.nameLabel.text = self.searchResultsArray[indexPath.row].value(forKey: "name") as? String
        cell.fullNameLabel.text = self.searchResultsArray[indexPath.row].value(forKey: "full_name") as? String
        cell.fullNameLabel.text = self.searchResultsArray[indexPath.row].value(forKey: "full_name") as? String
        if let watchers = self.searchResultsArray[indexPath.row].value(forKey: "watchers"){
            cell.watchersLabel.text = "Watchers: " + "\(watchers)"
        }
        if let forks = self.searchResultsArray[indexPath.row].value(forKey: "forks_count"){
            cell.commitLabel.text = "Forks: " + "\(forks)"
        }
        var ownerDict = NSDictionary()
        ownerDict = (searchResultsArray[indexPath.row] as AnyObject).value(forKey: "owner") as! NSDictionary
        if let avatar = ownerDict.value(forKey: "avatar_url") {
            cell.thumbImage.downloaded(from: avatar as! String)
            
        }
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 140
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "RepoDetailsViewController") as? RepoDetailsViewController
        vc?.repoDeatils = self.searchResultsArray[indexPath.row] as! [String : Any]
        self.navigationController?.pushViewController(vc!, animated: true)
    }
    
    @IBOutlet weak var searchTableView: UITableView!
    @IBOutlet weak var searchBar: UISearchBar!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        searchBar.delegate = self
        searchBar.placeholder = "Search here"
        let image : UIImage = UIImage(named: "mappr-logo")!
        let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 40, height: 40))
        imageView.contentMode = .scaleAspectFit
        imageView.image = image
        self.navigationItem.titleView = imageView
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        print("searchText \(searchText)")
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        print("searchText \(String(describing: searchBar.text))")
        if let search = searchBar.text{
            self.ServiceCall(searchString: search)
        }
    }
    func ServiceCall(searchString: String) {
         EZLoadingActivity.show("Mappr...", disableUI: false)
        let urlString = "https://api.github.com/search/repositories?q=\(searchString)&sort=stars&order=desc";
        let urlString1 = urlString.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
        let url : URL = URL(string: urlString1!)!;
        let session = URLSession.shared;
        var request = URLRequest(url: url);
        
        request.httpMethod = "GET";
        
        request.addValue("application/json", forHTTPHeaderField: "Content-Type");
        
        request.cachePolicy = NSURLRequest.CachePolicy.reloadIgnoringLocalCacheData;
        
        
        
        let task = session.dataTask(with: request, completionHandler: {
            
            (data, response, error) in
            
            guard let _:Data = data, let _:URLResponse = response , error == nil else {
                 EZLoadingActivity.hide()
                // Service Request Error
                
                print("Service Request Error");
                
                return;
                
            }
             EZLoadingActivity.hide()
            print("Response Data : \(data!)");
            
            let dataString = NSString(data: data!, encoding: String.Encoding.utf8.rawValue);
            
            print("Response String : \(dataString!)");
            
            // Parse Response Data
            
            do {
               
                
                let resultArray : NSDictionary = try JSONSerialization.jsonObject(with: data!, options: []) as! NSDictionary;
                
                // Success Response Data
                
                print("Result is : \(resultArray)");
                
                
                
                var itmesArray = [AnyObject]()
                
                DispatchQueue.main.async {
                
                itmesArray = resultArray.value(forKey: "items") as! [AnyObject]
                
                self.searchResultsArray = itmesArray
                self.searchTableView.reloadData()
                }
                
            
            } catch let error as NSError {
                
                // Failure Response Data
                
                print("Failed to load: \(error.localizedDescription)");
                
            }
            
        })
        
        
        
        task.resume();
        
    }
    

    


}
//View Setup
extension UIView{
    
    func setBorder(radius:CGFloat, color:UIColor = UIColor.darkGray) -> UIView{
        let roundView:UIView = self
        roundView.layer.cornerRadius = CGFloat(radius)
        roundView.layer.borderWidth = 2
        roundView.layer.borderColor = color.cgColor
        roundView.clipsToBounds = true
        return roundView
    }
}
//Image Download


extension UIImageView {
    func downloaded(from url: URL, contentMode mode: UIViewContentMode = .scaleAspectFit) {
        contentMode = mode
        URLSession.shared.dataTask(with: url) { data, response, error in
            guard
                let httpURLResponse = response as? HTTPURLResponse, httpURLResponse.statusCode == 200,
                let mimeType = response?.mimeType, mimeType.hasPrefix("image"),
                let data = data, error == nil,
                let image = UIImage(data: data)
                else { return }
            DispatchQueue.main.async() {
                self.image = image
            }
            }.resume()
    }
    func downloaded(from link: String, contentMode mode: UIViewContentMode = .scaleAspectFit) {
        guard let url = URL(string: link) else { return }
        downloaded(from: url, contentMode: mode)
    }
}

