//
//  ContributorViewController.swift
//  MapprTask
//
//  Created by Nandu Psycho on 11/10/18.
//  Copyright © 2018 Test. All rights reserved.
//

import UIKit

class ContributorViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
     var contributorsArray = Dictionary<String, Any>()
    var repoResultsArray = [Any]()
    var contributorDict = Dictionary<String, Any>()
    @IBOutlet weak var repoTableView: UITableView!
    @IBOutlet weak var avatarImage: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()
//        repos_url
        print(contributorsArray)
        
        if let repoListUrl =  (contributorsArray as AnyObject).value(forKey: "repos_url"){
            self.ServiceCall(contributor: repoListUrl as! String)
        }
        if let avatar = (contributorsArray as AnyObject).value(forKey: "avatar_url"){
            avatarImage.downloaded(from: avatar as! String)
            
        }


        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return repoResultsArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:GlobalTableViewCell = (tableView.dequeueReusableCell(withIdentifier: "contributor") as! GlobalTableViewCell?)!
        cell.nameLabel.text = (self.repoResultsArray[indexPath.row] as AnyObject).value(forKey: "name") as? String
        var ownerDict = NSDictionary()
        ownerDict = (repoResultsArray[indexPath.row] as AnyObject).value(forKey: "owner") as! NSDictionary
        if let avatar = ownerDict.value(forKey: "avatar_url") {
            cell.thumbImage.downloaded(from: avatar as! String)
            
        }
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "RepoDetailsViewController") as? RepoDetailsViewController
        vc?.repoDeatils = self.repoResultsArray[indexPath.row] as! [String : Any]
        self.navigationController?.pushViewController(vc!, animated: true)
    }
    
    func ServiceCall(contributor: String) {
         EZLoadingActivity.show("Mappr...", disableUI: false)
        let urlString = contributor;
        
        
        
        
        let url : URL = URL(string: urlString)!;
        
        let session = URLSession.shared;
        
        var request = URLRequest(url: url);
        
        request.httpMethod = "GET";
        
        request.addValue("application/json", forHTTPHeaderField: "Content-Type");
        
        request.cachePolicy = NSURLRequest.CachePolicy.reloadIgnoringLocalCacheData;
        
        
        
        let task = session.dataTask(with: request, completionHandler: {
            
            (data, response, error) in
            
            guard let _:Data = data, let _:URLResponse = response , error == nil else {
                EZLoadingActivity.hide()
                // Service Request Error
                
                print("Service Request Error");
                
                return;
                
            }
            EZLoadingActivity.hide()
            
            print("Response Data : \(data!)");
            
            let dataString = NSString(data: data!, encoding: String.Encoding.utf8.rawValue);
            
            print("Response String : \(dataString!)");
            
            // Parse Response Data
            
            do {
                let json = try JSONSerialization.jsonObject(with: data!)
                print("jnew son is array", json)
                if let jsonArray = json as? [Any] {
                    print("json is array", jsonArray)
                    DispatchQueue.main.async {
                        self.repoResultsArray = jsonArray
                        self.repoTableView.reloadData()
                    }
                } else if let jsonDictionary = json as? [String:Any] {
                    print("json is dictionary", jsonDictionary)
                } else {
                    print("This should never be displayed")
                }
 
            } catch let error as NSError {
                
                // Failure Response Data
                
                print("Failed to load: \(error.localizedDescription)");
                
            }
            
        })
        
        
        
        task.resume();
        
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
