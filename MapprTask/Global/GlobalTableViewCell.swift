//
//  GlobalTableViewCell.swift
//  MapprTask
//
//  Created by Nandu Psycho on 10/10/18.
//  Copyright © 2018 Test. All rights reserved.
//

import UIKit

class GlobalTableViewCell: UITableViewCell {

    @IBOutlet weak var commitLabel: UILabel!
    @IBOutlet weak var watchersLabel: UILabel!
    @IBOutlet weak var fullNameLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var baseView: UIView!
    @IBOutlet weak var thumbImage: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        baseView.setBorder(radius: 8, color: UIColor.darkGray)
        if thumbImage != nil{
            thumbImage.layer.cornerRadius = thumbImage.frame.size.height/2
            thumbImage.clipsToBounds = true
        }
        
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
