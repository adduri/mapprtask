//
//  GlobalwebViewController.swift
//  MapprTask
//
//  Created by Nandu Psycho on 11/10/18.
//  Copyright © 2018 Test. All rights reserved.
//

import UIKit
import WebKit
class GlobalwebViewController: UIViewController,WKNavigationDelegate, WKUIDelegate {
var urlString = ""
    @IBOutlet weak var webView: WKWebView!
    override func viewDidLoad() {
        super.viewDidLoad()
        let url = URL(string: urlString)
        let requestObj = URLRequest(url: url! as URL)
        webView.load(requestObj)
        webView.navigationDelegate = self
        EZLoadingActivity.show("Mappr...", disableUI: false)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
          EZLoadingActivity.hide()
    }
    
    func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
         EZLoadingActivity.hide()
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
