//
//  GlobalCollectionViewCell.swift
//  MapprTask
//
//  Created by Nandu Psycho on 10/10/18.
//  Copyright © 2018 Test. All rights reserved.
//

import UIKit

class GlobalCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var nameLbel: UILabel!
    @IBOutlet weak var baseView: UIView!
    @IBOutlet weak var avatarImage: UIImageView!
    
}
