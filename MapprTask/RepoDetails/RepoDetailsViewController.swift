//
//  RepoDetailsViewController.swift
//  MapprTask
//
//  Created by Nandu Psycho on 11/10/18.
//  Copyright © 2018 Test. All rights reserved.
//

import UIKit

class RepoDetailsViewController: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return contributorsArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Repo", for: indexPath as IndexPath) as! GlobalCollectionViewCell
//        login
        cell.nameLbel.text = (self.contributorsArray[indexPath.row] as AnyObject).value(forKey: "login") as? String
        let avatarImage = (self.contributorsArray[indexPath.row] as AnyObject).value(forKey: "avatar_url") as? String
        cell.avatarImage.downloaded(from: avatarImage!)
        
        return cell

        
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "ContributorViewController") as? ContributorViewController
        
        vc?.contributorsArray = (self.contributorsArray[indexPath.row] as AnyObject) as! [String : Any]
        self.navigationController?.pushViewController(vc!, animated: true)
        
    }
    var contributorsArray = [Any]()
    var repoDeatils = Dictionary<String, Any>()
    @IBOutlet weak var baseImage: UIImageView!
    @IBOutlet weak var descTextView: UITextView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var projectLonkButton: UIButton!
    @IBOutlet weak var contributorCollectionView: UICollectionView!

    override func viewDidLoad() {
        super.viewDidLoad()
        nameLabel.text = (repoDeatils as AnyObject).value(forKey: "name") as? String
        
        nameLabel.text = (repoDeatils as AnyObject).value(forKey: "name") as? String
        projectLonkButton.setTitle((repoDeatils as AnyObject).value(forKey: "html_url") as? String, for: .normal)
        descTextView.text = (repoDeatils as AnyObject).value(forKey: "description") as? String
        let contributorLink = (repoDeatils as AnyObject).value(forKey: "contributors_url") as? String
        self.ServiceCall(contributor: contributorLink!)
        var ownerDict = NSDictionary()
        ownerDict = (repoDeatils as AnyObject).value(forKey: "owner") as! NSDictionary
        if let avatar = ownerDict.value(forKey: "avatar_url") {
            baseImage.downloaded(from: avatar as! String)
            
        }
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    func ServiceCall(contributor: String) {
         EZLoadingActivity.show("Mappr...", disableUI: false)
        let urlString = contributor;
        
        
        
        
        let url : URL = URL(string: urlString)!;
        
        let session = URLSession.shared;
        
        var request = URLRequest(url: url);
        
        request.httpMethod = "GET";
        
        request.addValue("application/json", forHTTPHeaderField: "Content-Type");
        
        request.cachePolicy = NSURLRequest.CachePolicy.reloadIgnoringLocalCacheData;
        
        
        
        let task = session.dataTask(with: request, completionHandler: {
            
            (data, response, error) in
             EZLoadingActivity.hide()
            guard let _:Data = data, let _:URLResponse = response , error == nil else {
                
                // Service Request Error
                
                print("Service Request Error");
                
                return;
                
            }
            
            print("Response Data : \(data!)");
            
            let dataString = NSString(data: data!, encoding: String.Encoding.utf8.rawValue);
            
            print("Response String : \(dataString!)");
            
            // Parse Response Data
            
            do {
                let json = try JSONSerialization.jsonObject(with: data!)
                print("jnew son is array", json)
                if let jsonArray = json as? [Any] {
                    print("json is array", jsonArray)
                    DispatchQueue.main.async {
                        self.contributorsArray = jsonArray
                        self.contributorCollectionView.reloadData()
                    }
                } else if let jsonDictionary = json as? [String:Any] {
                    print("json is dictionary", jsonDictionary)
                } else {
                    print("This should never be displayed")
                }
                
                // Success Response Data
                
                
                
                
                
                
                
                
            } catch let error as NSError {
                
                // Failure Response Data
                
                print("Failed to load: \(error.localizedDescription)");
                
            }
            
        })
        
        
        
        task.resume();
        
    }
    @IBAction func repoLinkAction(_ sender: UIButton) {
        let urlStr = (repoDeatils as AnyObject).value(forKey: "html_url") as? String
        let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "GlobalwebViewController") as? GlobalwebViewController
        vc?.urlString = urlStr!
        self.navigationController?.pushViewController(vc!, animated: true)
        
    }
}
